import React from 'react'

import './main.css'

const Home = () => {
    return (
        <div className={'home'}>
            <div className={'container'}>
                <div className={'row'}>
                    <div className={'col-xl-12'}>
                        <h1>Home</h1>

                        <a href={'/youtube'}>
                            <div className={'card'}>
                                <div className={'card-body'}>
                                    <p>Youtube</p>
                                </div>
                            </div>
                        </a>

                        <a href={'/zoom'}>
                            <div className={'card'}>
                                <div className={'card-body'}>
                                    <p>Zoom</p>
                                </div>
                            </div>
                        </a>

                        <a href={'/teams'}>
                            <div className={'card'}>
                                <div className={'card-body'}>
                                    <p>Teams</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home
