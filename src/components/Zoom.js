import React from 'react'

import './main.css'

const Zoom = () => {
    return (
        <div className={"zoom"}>
            <div className={'container'}>
                <div className={'row'}>
                <div className={'col-xl-12'}>
                    <h1>Zoom Embed</h1>
                    <a href={'/'}>Back to Home</a>
                    <div className="iframe-container">
                    <iframe allow="microphone; camera" src="https://zoom.us/wc/join/85161849595" frameBorder="0"></iframe>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}

export default Zoom
