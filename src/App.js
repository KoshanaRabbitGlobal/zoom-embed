import './App.css';

import Zoom from '../src/components/Zoom'
import Teams from './components/Teams';
import Youtube from './components/Youtube';
import Home from './components/Home';

import { BrowserRouter as Router , Switch, Route } from 'react-router-dom'

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path={'/teams'}>
            <Teams/>
          </Route>
          <Route path={'/zoom'}>
            <Zoom/>
          </Route>
          <Route path={'/youtube'}>
            <Youtube/>
          </Route>
          <Route path={'/'}>
            <Home/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
